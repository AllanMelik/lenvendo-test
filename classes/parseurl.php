<?php

/*
 * detect video values by url 
 * */
class ParseURL
{
    /*
     * base hosting values
     * */
    protected $videoHostings = [
        'Youtube' => [
            'hosts' => [
                'youtube.com',
                'youtu.be',
            ],
            'methods' => [
                'youtube.com' =>
                    [
                        'method' => 'queryString',
                        'queryParam' => 'v',
                    ],
                'youtu.be' =>
                    [
                        'method' => 'path',
                    ],
            ],
            'template' => '<iframe width="560" height="315" src="https://www.youtube.com/embed/TPL_VIDEO_ID" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
        ],
        'Vimeo' => [
            'hosts' => [
                'vimeo.com'
            ],
            'methods' => [
                'vimeo.com' =>
                    [
                        'method' => 'path',
                    ],
            ],
            'template' => '<iframe src="https://player.vimeo.com/video/TPL_VIDEO_ID" width="560" height="315" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>'
        ]
    ];

    protected $url; // input url
    protected $videoHostingName; // result video hosting name
    protected $videoHostingMethod; // making values method
    protected $videoHostingHost; // video hosting host url
    protected $videoID; // result video ID
    protected $videoIframe; // result video iframe
    protected $arUrlParsed = []; // parsed url array

    function __construct($url)
    {
        $this->url = $url;
        $this->prepareURL();
    }

    /*
     * fixing supposed problems
     * */
    function prepareURL()
    {

        $this->url = trim($this->url);
        $this->url = mb_strtolower($this->url);

        return false;
    }

    /*
     * parsing base values from url
     * */
    function processParsed()
    {

        $this->arUrlParsed['host'] = str_replace('www.', '', $this->arUrlParsed['host']);
        $this->arUrlParsed['path'] = str_replace('/', '', $this->arUrlParsed['path']);
        parse_str($this->arUrlParsed['query'], $this->arUrlParsed['query']);

        return false;
    }

    /*
     * getting Hosting values by parsed url
     * */
    function getHosting()
    {

        if (!$this->arUrlParsed || !$this->arUrlParsed['host']) return false;

        foreach ($this->videoHostings as $hName => $hValues) {

            if (in_array($this->arUrlParsed['host'], $hValues['hosts'])) {
                $this->videoHostingName = $hName;
                $this->videoHostingHost = $this->arUrlParsed['host'];
                $this->videoHostingMethod = $hValues['methods'][$this->arUrlParsed['host']]['method'];
            }
        }

        return false;
    }

    /*
     * detecting video ID
     * */
    function getVideoID()
    {

        if (!$this->arUrlParsed || !$this->videoHostingName || !$this->videoHostingMethod) return false;

        switch ($this->videoHostingMethod) {

            case 'queryString':
                $this->videoID = $this->arUrlParsed['query'][$this->videoHostings[$this->videoHostingName]['methods'][$this->videoHostingHost]['queryParam']];

                break;

            case 'path':

                $this->videoID = $this->arUrlParsed['path'];

                break;
        }

        return false;
    }

    /*
 * making video iframe
 * */
    function getIframe()
    {

        if (!$this->videoID) return false;
        $this->videoIframe = str_replace('TPL_VIDEO_ID', $this->videoID, $this->videoHostings[$this->videoHostingName]['template']);

        return false;
    }

    /*
     * takes url, makes all video values if it is possible
     * */
    function processUrl($url)
    {

        if (!$url) return false;

        $this->arUrlParsed = parse_url($url);
        $this->processParsed();
        $this->getHosting();
        $this->getVideoID();
        $this->getIframe();
        if (
            $this->videoHostingName &&
            $this->videoID &&
            $this->videoIframe
        ) {


            return [
                'videoHostingName' => $this->videoHostingName,
                'videoID' => $this->videoID,
                'videoIframe' => $this->videoIframe,
            ];
        }

        return false;
    }
}

?>